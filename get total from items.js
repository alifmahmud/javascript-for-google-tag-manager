var items =  [
    {
      name: "Assorted Coffee",
      id: "168",
      price: "500",
      quantity: "5",
    }
];

var totalPrice = 0;

for(var i = 0; i < items.length; i++){
    var qty = 1;

    var price = Number(items[i].price);
    if(items[i].quantity){
       qty = Number(items[i].quantity);
    }

    totalPrice += price*qty;
}

console.log(totalPrice);


