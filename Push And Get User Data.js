// push user data in the data layer and also in the browser local storage



/*
learn how to get user data from the input fields or from any elements

https://www.w3docs.com/snippets/javascript/how-to-get-the-value-of-text-input-field-using-javascript.html

*/



var name = "Alif Mahmud"; // replace it with our dom selector 
var phone = "01743396192"; // replace it with our dom selector 
var email = "info@alifmahmud.com"; // replace it with our dom selector 


var userData = {
    firstName: name.split(" ")[0],
    lastName: name.split(" ")[1] || "",
    name: name,
    phone: phone,
    email: email,
}

localStorage.setItem("userData", JSON.stringify(userData))


window.dataLayer = window.dataLayer || [];
window.dataLayer.push({
    event: "userData",
    userData: userData,
})


console.log(userData);






// ger user data from the local storage and push the data layer event

var getUserData = localStorage.getItem("userData");

console.log(JSON.parse(getUserData));

window.dataLayer = window.dataLayer || [];
window.dataLayer.push({
    event: "userData",
    userData: JSON.parse(getUserData),
})



// if you want to remove the data

localStorage.removeItem("userData");










