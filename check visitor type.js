// setup visitor type browser only
var getUserType = localStorage.getItem("userType");

if(getUserType){
   localStorage.setItem("userType", "Returning");
}else{
    localStorage.setItem("userType", "New");
    window.dataLayer = window.dataLayer || [];
    window.dataLayer.push({
        event: "firstVisit",
    })
}


//https://gitlab.com/alifmahmud/javascript-for-google-tag-manager/
